# dos-script-war-deployment

DOS batch for "manual" deployment of a WAR in Tomcat

The bat does the following:

- Stops Tomcat service
- Removes Tomcat's work directory for the context
- Moves currently deployed context directory and war file to a backup folder
- Copies the new WAR to the Tomcat's deployment directory
- Starts Tomcat service

Edit the bat to change folder names, file names and service name for matching your environment.
