@echo off
REM #######################################
REM # Script for WAR deployment
REM # IT zone 2021
REM #######################################
 
set LOG_FILE=E:\WORK\DEPLOY\deploy.log
set BACKUP_DIR=E:\WORK\DEPLOY\BACKUP\
set WAR_TO_DEPLOY=E:\WORK\DEPLOY\ROOT.war
set CONTEXT_DIR=ROOT
set CONTEXT_WAR=ROOT.war
set DIR_TOMCAT_WEBAPPS="C:\Program Files\Apache Software Foundation\Tomcat 8.5\webapps\"
set DIR_TOMCAT_WORK="C:\Program Files\Apache Software Foundation\Tomcat 8.5\work\Catalina\localhost\ROOT\"
set TOMCAT_SERVICE="Tomcat_8_5"

set TIMESTAMP=%date:~6,4%%date:~3,2%%date:~0,2%-%time:~0,2%%time:~3,2%%time:~6,2%
set TIMESTAMP=%TIMESTAMP: =0%

echo Deploying %WAR_TO_DEPLOY% into context %CONTEXT_DIR%. 
echo Backup to %BACKUP_DIR%%TIMESTAMP%. 

echo ----------  NEW DEPLOYMENT %TIMESTAMP%  ---------- >> %LOG_FILE%

echo [%time:~0,2%:%time:~3,2%:%time:~6,2%] Stopping Tomcat >> %LOG_FILE%
net stop %TOMCAT_SERVICE% >> %LOG_FILE%

echo [%time:~0,2%:%time:~3,2%:%time:~6,2%] Cleaning Tomcat work directory >> %LOG_FILE%
rd /s /q %DIR_TOMCAT_WORK%  >> %LOG_FILE%

echo [%time:~0,2%:%time:~3,2%:%time:~6,2%] Saving backup >> %LOG_FILE%
mkdir %BACKUP_DIR%%TIMESTAMP%  >> %LOG_FILE%
xcopy %DIR_TOMCAT_WEBAPPS%%CONTEXT_DIR% %BACKUP_DIR%%TIMESTAMP%\%CONTEXT_DIR%\ /E/H/Q/Y  >> %LOG_FILE%
rmdir /S/Q %DIR_TOMCAT_WEBAPPS%%CONTEXT_DIR%  >> %LOG_FILE%
move /Y %DIR_TOMCAT_WEBAPPS%%CONTEXT_WAR% %BACKUP_DIR%%TIMESTAMP%\  >> %LOG_FILE%

echo [%time:~0,2%:%time:~3,2%:%time:~6,2%] Copying new WAR to deploy directory >> %LOG_FILE%
copy %WAR_TO_DEPLOY% %DIR_TOMCAT_WEBAPPS%\%CONTEXT_WAR%  >> %LOG_FILE%
move %WAR_TO_DEPLOY% %BACKUP_DIR%\%TIMESTAMP%_%CONTEXT_WAR%  >> %LOG_FILE%

echo [%time:~0,2%:%time:~3,2%:%time:~6,2%] Starting Tomcat >> %LOG_FILE%
net start %TOMCAT_SERVICE% >> %LOG_FILE%

echo ----------  DEPLOYMENT FINISHED - %time:~0,2%:%time:~3,2%:%time:~6,2%  ---------- >> %LOG_FILE%
echo The deployment has finished. See %LOG_FILE% for details.
